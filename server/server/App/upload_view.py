from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from rest_framework import viewsets, views
from rest_framework import permissions
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
import numpy as np
import json


class UploadFileView(views.APIView):
    """
    API endpoint that allows file upload.
    """
    parser_classes = (FileUploadParser,)

    def write_to_file(self, data, filename):
        from django.core.files.storage import default_storage

        with default_storage.open(filename, "wb+") as f:
            for chunk in data.chunks():
                f.write(chunk)

    def from_flac_to_wav(self, filename, output_filename):
        # import ffmpeg
        # stream = ffmpeg.input(filename)
        # stream = ffmpeg.output(stream, "-i", output_filename)
        # ffmpeg.run(stream)
        import os
        os.remove(output_filename)
        os.system('ffmpeg -i {} {}'.format(filename, output_filename))

    def file_to_spectrograms(self, wave_file, n_mels=128, len=256, hop_length=431):
        import librosa
        y, sr = librosa.load(wave_file)
        mel_spec = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=n_mels, hop_length=hop_length)
        mel_spec_db = librosa.power_to_db(mel_spec, ref=np.max)
        step = 51
        for i in range(int((mel_spec_db.shape[1] - len) / step)):
          yield mel_spec_db[:, i*step:len + i*step]

    def normalize(self, spectrogram):
        return (spectrogram - alex_net_mean_normalization) / alex_net_std_normalization

    def put(self, request, filename, format=None):
        file_obj = request.FILES['file']
        components = filename.split(".")
        if components[len(components) - 1] == "flac":
            filepath = str(file_obj)
            self.write_to_file(file_obj, filepath)
            wav_filepath = filepath+".wav"
            self.from_flac_to_wav(filepath, wav_filepath)

            normalized_spectrograms = []
            for spectrogram in self.file_to_spectrograms(wav_filepath):
              print(spectrogram.shape)
              normalized = self.normalize(spectrogram)
              normalized_spectrograms.append(normalized)


            results = alex_net.predict(normalized_spectrograms)
            result = [0] * len(classifier_dictionary3)
            for r in results:
              for (idx, prob) in enumerate(r):
                result[idx] += prob

            # for i in range(len(result)):
            #   result[i] = result[i] / float(len(normalized_spectrograms))

            result = sorted(enumerate(result), key=lambda x: -x[1])

            for (idx, prob) in result[:5]:
                print(idx, prob, classifier_dictionary3[str(idx)])

            json_response = {}
            for (idx, prob) in result:
              json_response[classifier_dictionary3[str(idx)]] = float(prob)

            return HttpResponse(json.dumps(json_response))
        return Response(status=400)

    # do some stuff with uploaded file


def create_alex_net(input_shape, output_size):
    import tensorflow as tf
    from tensorflow.keras import Model
    from tensorflow.keras.layers import Dropout, Dense, Conv2D, Flatten, LeakyReLU, BatchNormalization, ZeroPadding2D
    from tensorflow.keras.layers import Input

    inp = Input(shape=input_shape)
    # embedding = alex_net_arhitecture(input_shape)(inp)

    model_pipeline = Conv2D(32, kernel_size=3, strides=2, padding="same")(inp)
    model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
    model_pipeline = Dropout(.25)(model_pipeline)

    model_pipeline = Conv2D(64, kernel_size=3, strides=2, padding="same")(model_pipeline)
    model_pipeline = ZeroPadding2D(padding=((0, 1), (0, 1)))(model_pipeline)
    model_pipeline = BatchNormalization(momentum=.8)(model_pipeline)
    model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
    model_pipeline = Dropout(.25)(model_pipeline)

    model_pipeline = Conv2D(128, kernel_size=3, strides=2, padding="same")(model_pipeline)
    model_pipeline = BatchNormalization(momentum=.8)(model_pipeline)
    model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
    model_pipeline = Dropout(.25)(model_pipeline)

    model_pipeline = Conv2D(256, kernel_size=3, strides=1, padding="same")(model_pipeline)
    model_pipeline = BatchNormalization(momentum=.8)(model_pipeline)
    model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
    model_pipeline = Dropout(.25)(model_pipeline)
    model_pipeline = Flatten()(model_pipeline)

    embedding = Dense(300, activation="relu")(model_pipeline)
    model_pipeline = Dropout(.25)(embedding)

    output = Dense(output_size, activation="softmax")(model_pipeline)

    model = Model(inp, output)
    return model


# alex_net = create_alex_net((128, 431, 1), 57) # 0, 1
alex_net = create_alex_net((128, 256, 1), 57)
alex_net.summary()
alex_net.load_weights("models/alex-net-0.3.h5")
alex_net_mean_normalization = np.load("models/mean-normalization-0.3.npy")
alex_net_std_normalization = np.load("models/std-normalization-0.3.npy")

classifier_dictionary_alex_1 = {
 0: 'The Black Eyed Peas - Pump It.wav',
 1: 'Michael Jackson - Billie Jean.wav',
 2: 'Christina Aguilera - Candyman.wav',
 3: 'Shakira - Waka Waka.wav',
 4: 'LMFAO - Sexy and I Know It.wav',
 5: 'Lady Gaga - Bad Romance.wav',
 6: 'Rihanna - Diamonds.wav',
 7: 'Timbaland - Apologize ft. OneRepublic.wav',
 8: 'Schubert - Serenade.wav',
 9: 'Verdi - La Traviata.wav',
 10: 'Wolfgang Amadeus Mozzart - Mala nocna muzika.wav',
 11: 'Carl Orff (André Rieu) - O Fortuna.wav',
 12: 'Frédéric Chopin - Prelude in E-Minor.wav',
 13: 'Chopin - Nocturne.wav',
 14: 'Andrea Bocelli & Sarah Brightman  - Time to say goodbye.wav',
 15: 'Beethoven - 5th Symphony.wav',
 16: 'Georges Bizet - Carmen.wav',
 17: 'André Rieu - Figaro Cavatina.wav',
 18: 'Ansambel Tomaža Rota - Tam dol na ravnem polju.wav',
 19: 'MAMBO KINGS - BALERINA.wav',
 20: 'Lojze Slak - Lunca.wav',
 21: 'Ansambel Petra Finka - Morska pravljica.wav',
 22: 'Faraoni - Ne bom pozabil na stare čase.wav',
 23: 'Peter Lovšin - Sam en majhen poljub.wav',
 24: 'Vlado Kreslin - Z Gorickega V Piran.wav',
 25: 'Ansambel bratov Avsenik - Tam kjer murke cveto.wav',
 26: 'Ansambel Toneta Rusa - Čas bi zavrtel nazaj.wav',
 27: 'Lojze Slak - Poštar.wav',
 28: 'Lojze Slak - Srnjak.wav',
 29: 'Bog foot mama - Lovro.wav',
 30: 'ANS. FRANCA MIHELIČA - VESELI RIBNČAN.wav',
 31: 'Miran Rudan - Na nebo.wav',
 32: 'Martin Krpan - Je v Šiški še kaj odprtega.wav',
 33: 'Mladi Dolenjci - Meni ni za bogatijo.wav',
 34: 'Martin Krpan - Od višine se zvrti.wav',
 35: 'Zmelkoow - Bit.wav',
 36: 'Iggy Pop - The Passenger.wav',
 37: 'Queen - We Are The Champions.wav',
 38: 'Survivor - Eye Of The Tiger.wav',
 39: 'Nirvana - Smells Like Teen Spirit.wav',
 40: 'The Rolling Stones - Paint It, Black.wav',
 41: 'AC:DC - Thunderstruck.wav',
 42: 'Queen – Bohemian Rhapsody.wav',
 43: "Bon Jovi - Livin' On A Prayer.wav",
 44: 'Dire Straits - Sultans Of Swing.wav',
 45: 'Ram Jam - Black Betty.wav',
 46: 'Dr. Dre feat. Snoop Dogg - Still Dre.wav',
 47: "Elvis Presley - Can't Help Falling In Love.wav",
 48: 'Remember The Name - Fort Minor.wav',
 49: 'Eminem - Without Me.wav',
 50: 'Coolio - Gangsters Paradise.wav',
 51: 'Cypress Hill - Hits from the Bong.wav',
 52: 'Eminem - Lose Yourself.wav',
 53: '50 Cent - Candy Shop.wav',
 54: 'Eminem - The Real Slim Shady.wav',
 55: 'SNOOP DOGG - SMOKE WEED EVERYDAY.wav',
 56: 'Beyonce - Halo.wav'
}

# classifier_dictionary = {
#  0: '-',
#  1: 'Michael Jackson - Billie Jean.wav',
#  2: 'Christina Aguilera - Candyman.wav',
#  3: 'Shakira - Waka Waka.wav',
#  4: 'LMFAO - Sexy and I Know It.wav',
#  5: 'Lady Gaga - Bad Romance.wav',
#  6: 'Rihanna - Diamonds.wav',
#  7: 'Timbaland - Apologize ft. OneRepublic.wav',
#  8: 'Schubert - Serenade.wav',
#  9: 'Verdi - La Traviata.wav',
#  10: 'Wolfgang Amadeus Mozzart - Mala nocna muzika.wav',
#  11: 'Carl Orff (André Rieu) - O Fortuna.wav',
#  12: 'Frédéric Chopin - Prelude in E-Minor.wav',
#  13: 'Chopin - Nocturne.wav',
#  14: 'Andrea Bocelli & Sarah Brightman  - Time to say goodbye.wav',
#  15: 'Beethoven - 5th Symphony.wav',
#  16: 'Georges Bizet - Carmen.wav',
#  17: 'André Rieu - Figaro Cavatina.wav',
#  18: 'Ansambel Tomaža Rota - Tam dol na ravnem polju.wav',
#  19: 'MAMBO KINGS - BALERINA.wav',
#  20: 'Lojze Slak - Lunca.wav',
#  21: 'Ansambel Petra Finka - Morska pravljica.wav',
#  22: 'Faraoni - Ne bom pozabil na stare čase.wav',
#  23: 'Peter Lovšin - Sam en majhen poljub.wav',
#  24: 'Vlado Kreslin - Z Gorickega V Piran.wav',
#  25: 'Ansambel bratov Avsenik - Tam kjer murke cveto.wav',
#  26: 'Ansambel Toneta Rusa - Čas bi zavrtel nazaj.wav',
#  27: 'Lojze Slak - Poštar.wav',
#  28: 'Lojze Slak - Srnjak.wav',
#  29: 'Bog foot mama - Lovro.wav',
#  30: 'ANS. FRANCA MIHELIČA - VESELI RIBNČAN.wav',
#  31: 'Miran Rudan - Na nebo.wav',
#  32: 'Martin Krpan - Je v Šiški še kaj odprtega.wav',
#  33: 'Mladi Dolenjci - Meni ni za bogatijo.wav',
#  34: 'Martin Krpan - Od višine se zvrti.wav',
#  35: 'Zmelkoow - Bit.wav',
#  36: 'Iggy Pop - The Passenger.wav',
#  37: 'Queen - We Are The Champions.wav',
#  38: 'Survivor - Eye Of The Tiger.wav',
#  39: 'Nirvana - Smells Like Teen Spirit.wav',
#  40: 'The Rolling Stones - Paint It, Black.wav',
#  41: 'AC:DC - Thunderstruck.wav',
#  42: 'Queen – Bohemian Rhapsody.wav',
#  43: "Bon Jovi - Livin' On A Prayer.wav",
#  44: 'Dire Straits - Sultans Of Swing.wav',
#  45: 'Ram Jam - Black Betty.wav',
#  46: 'Dr. Dre feat. Snoop Dogg - Still Dre.wav',
#  47: "Elvis Presley - Can't Help Falling In Love.wav",
#  48: 'Beyoncé - Halo.wav',
#  49: 'Remember The Name - Fort Minor.wav',
#  50: 'Eminem - Without Me.wav',
#  51: 'Coolio - Gangsters Paradise.wav',
#  52: 'Cypress Hill - Hits from the Bong.wav',
#  53: 'Eminem - Lose Yourself.wav',
#  54: '50 Cent - Candy Shop.wav',
#  55: 'Eminem - The Real Slim Shady.wav',
#  56: 'SNOOP DOGG - SMOKE WEED EVERYDAY.wav'
# }

classifier_dictionary3 = {"0": "The Black Eyed Peas - Pump It.wav", "1": "Michael Jackson - Billie Jean.wav", "2": "Christina Aguilera - Candyman.wav", "3": "Shakira - Waka Waka.wav", "4": "LMFAO - Sexy and I Know It.wav", "5": "Lady Gaga - Bad Romance.wav", "6": "Rihanna - Diamonds.wav", "7": "Timbaland - Apologize ft. OneRepublic.wav", "8": "Schubert - Serenade.wav", "9": "Verdi - La Traviata.wav", "10": "Wolfgang Amadeus Mozzart - Mala nocna muzika.wav", "11": "Carl Orff (Andr\u00e9 Rieu) - O Fortuna.wav", "12": "Fr\u00e9d\u00e9ric Chopin - Prelude in E-Minor.wav", "13": "Chopin - Nocturne.wav", "14": "Andrea Bocelli & Sarah Brightman  - Time to say goodbye.wav", "15": "Beethoven - 5th Symphony.wav", "16": "Georges Bizet - Carmen.wav", "17": "Andr\u00e9 Rieu - Figaro Cavatina.wav", "18": "Ansambel Toma\u017ea Rota - Tam dol na ravnem polju.wav", "19": "MAMBO KINGS - BALERINA.wav", "20": "Lojze Slak - Lunca.wav", "21": "Ansambel Petra Finka - Morska pravljica.wav", "22": "Faraoni - Ne bom pozabil na stare \u010dase.wav", "23": "Peter Lov\u0161in - Sam en majhen poljub.wav", "24": "Vlado Kreslin - Z Gorickega V Piran.wav", "25": "Ansambel bratov Avsenik - Tam kjer murke cveto.wav", "26": "Ansambel Toneta Rusa - \u010cas bi zavrtel nazaj.wav", "27": "Lojze Slak - Po\u0161tar.wav", "28": "Lojze Slak - Srnjak.wav", "29": "Bog foot mama - Lovro.wav", "30": "ANS. FRANCA MIHELI\u010cA - VESELI RIBN\u010cAN.wav", "31": "Miran Rudan - Na nebo.wav", "32": "Martin Krpan - Je v \u0160i\u0161ki \u0161e kaj odprtega.wav", "33": "Mladi Dolenjci - Meni ni za bogatijo.wav", "34": "Martin Krpan - Od vi\u0161ine se zvrti.wav", "35": "Zmelkoow - Bit.wav", "36": "Iggy Pop - The Passenger.wav", "37": "Queen - We Are The Champions.wav", "38": "Survivor - Eye Of The Tiger.wav", "39": "Nirvana - Smells Like Teen Spirit.wav", "40": "The Rolling Stones - Paint It, Black.wav", "41": "AC:DC - Thunderstruck.wav", "42": "Queen \u2013 Bohemian Rhapsody.wav", "43": "Bon Jovi - Livin' On A Prayer.wav", "44": "Dire Straits - Sultans Of Swing.wav", "45": "Ram Jam - Black Betty.wav", "46": "Dr. Dre feat. Snoop Dogg - Still Dre.wav", "47": "Elvis Presley - Can't Help Falling In Love.wav", "48": "Remember The Name - Fort Minor.wav", "49": "Eminem - Without Me.wav", "50": "Coolio - Gangsters Paradise.wav", "51": "Cypress Hill - Hits from the Bong.wav", "52": "Eminem - Lose Yourself.wav", "53": "50 Cent - Candy Shop.wav", "54": "Eminem - The Real Slim Shady.wav", "55": "SNOOP DOGG - SMOKE WEED EVERYDAY.wav", "56": "Beyonce - Halo.wav"}