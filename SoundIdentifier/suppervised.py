# -*- coding: utf-8 -*-
"""Suppervised.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1FlFgV6uRNm26UnoMhkboBS9cnh1rdeKA
"""

!pip install wandb -qqq
import wandb
wandb.login()

import os
import json
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
import matplotlib.pyplot as plt

from typing import List, Tuple, NewType
# from pydub import AudioSegment
from scipy.io import wavfile

from tensorflow.keras import Sequential, Model
from tensorflow.keras.layers import Dropout, Dense, Conv2D, Flatten, LeakyReLU, BatchNormalization, ZeroPadding2D
from tensorflow.keras.layers import Input, ZeroPadding2D, MaxPooling2D, Activation, Add, AveragePooling2D
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.metrics import sparse_categorical_accuracy
from tensorflow.keras.regularizers import l2

from tensorflow.keras.utils import to_categorical
from wandb.keras import WandbCallback

from google.colab import drive
drive.mount('/content/drive')



BASE_FOLDER = "/content/drive/My Drive/ML/shazam/"
DATA_FOLDER = BASE_FOLDER + "data/"


SONG_FOLDER = DATA_FOLDER + "songs/"
X_FILE_PATH = SONG_FOLDER + "X.npy"
y_FILE_PATH = SONG_FOLDER + "y.npy"
lunca_FILE_PATH = SONG_FOLDER + "lunca (1).npy"
AMBIENT_FOLDER = DATA_FOLDER + "Ambient noise/"

TEMPORARY_FILE = DATA_FOLDER + "temporary.wav"
MODEL_FILE_PATH = BASE_FOLDER + "models/alex-net.h5"





# Load a wav file
def get_wav_info(wav_file):
  rate, data = wavfile.read(wav_file)
  return rate, data

# Calculate and plot spectrogram for a wav audio file
def graph_spectrogram(wav_file):
  rate, data = get_wav_info(wav_file)
  nfft = 200 # Length of each window segment
  fs = 8000 # Sampling frequencies
  noverlap = 120 # Overlap between windows
  nchannels = data.ndim
  if nchannels == 1:
      pxx, freqs, bins, im = plt.specgram(data, nfft, fs, noverlap = noverlap)
  elif nchannels == 2:
      pxx, freqs, bins, im = plt.specgram(data[:,0], nfft, fs, noverlap = noverlap)
  return pxx

Dataset = NewType("Dataset", Tuple[List[np.ndarray], List[str]])

def split_wav_file_on_fragments(wav_file_path: str, duration_ms: int, padding_ms=40000) -> np.ndarray:
	audio = AudioSegment.from_wav(wav_file_path)
	start, end = 0, duration_ms
	length = len(audio)
	fragment_index = 1

	fragments = list()

	while end < length:
		fragment = audio[start:end]
		fragment.export(TEMPORARY_FILE, format="wav")
		fragments.append(graph_spectrogram(TEMPORARY_FILE))

		start, end = start + padding_ms, end + padding_ms
		fragment_index += 1

	return np.array(fragments)


def load_folder_songs(folder: str) -> Dataset:
	song_files = [file for file in os.listdir(folder) if file.endswith(".wav")]

	X: List[np.ndarray] = list()
	y: List[str] = list()

	for song_file_name in song_files:
		song_fragments = split_wav_file_on_fragments(folder + song_file_name, 5000)
		X.extend(song_fragments)
		y.extend([song_file_name] * len(song_fragments))

	return X, y


def load_songs(folder: str) -> Dataset:
	print("Loading folder {}".format(folder))
	X, y = load_folder_songs(folder)

	for sub_folder in os.listdir(folder):
		sub_folder_path = "{}{}/".format(folder, sub_folder)
		if not os.path.isdir(sub_folder_path) or sub_folder == "Classical":
			continue

		sub_songs, sub_names = load_songs(sub_folder_path)

		X.extend(sub_songs)
		y.extend(sub_names)

	return X, y

X, y = load_songs(SONG_FOLDER)

X = np.array(X)
y = np.array(y)

mean = np.mean(X, axis=(0, 2))
std = np.std(X, axis=(0, 2))
#X = (X - mean[None, :, None]) / std[None, :, None]

r = (X - mean[None, :, None]) / std[None, :, None]

len(set(y))

model = tf.keras.applications.ResNet50(
  include_top=False,
  # weights="imagenet",
  input_tensor=None,
  input_shape=(101, 5998, 1),
  pooling=None,
  classes=56
)

model.summary()

model.predict()

class BasicBlock(tf.keras.layers.Layer):

    def __init__(self, filter_num, stride=1):
        super(BasicBlock, self).__init__()
        self.conv1 = tf.keras.layers.Conv2D(filters=filter_num,
                                            kernel_size=(3, 3),
                                            strides=stride,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.conv2 = tf.keras.layers.Conv2D(filters=filter_num,
                                            kernel_size=(3, 3),
                                            strides=1,
                                            padding="same")
        self.bn2 = tf.keras.layers.BatchNormalization()
        if stride != 1:
            self.downsample = tf.keras.Sequential()
            self.downsample.add(tf.keras.layers.Conv2D(filters=filter_num,
                                                       kernel_size=(1, 1),
                                                       strides=stride))
            self.downsample.add(tf.keras.layers.BatchNormalization())
        else:
            self.downsample = lambda x: x

    def call(self, inputs, training=None, **kwargs):
        residual = self.downsample(inputs)

        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.conv2(x)
        x = self.bn2(x, training=training)

        output = tf.nn.relu(tf.keras.layers.add([residual, x]))

        return output


class BottleNeck(tf.keras.layers.Layer):
    def __init__(self, filter_num, stride=1):
        super(BottleNeck, self).__init__()
        self.conv1 = tf.keras.layers.Conv2D(filters=filter_num,
                                            kernel_size=(1, 1),
                                            strides=1,
                                            padding='same')
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.conv2 = tf.keras.layers.Conv2D(filters=filter_num,
                                            kernel_size=(3, 3),
                                            strides=stride,
                                            padding='same')
        self.bn2 = tf.keras.layers.BatchNormalization()
        self.conv3 = tf.keras.layers.Conv2D(filters=filter_num * 4,
                                            kernel_size=(1, 1),
                                            strides=1,
                                            padding='same')
        self.bn3 = tf.keras.layers.BatchNormalization()

        self.downsample = tf.keras.Sequential()
        self.downsample.add(tf.keras.layers.Conv2D(filters=filter_num * 4,
                                                   kernel_size=(1, 1),
                                                   strides=stride))
        self.downsample.add(tf.keras.layers.BatchNormalization())

    def call(self, inputs, training=None, **kwargs):
        residual = self.downsample(inputs)

        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.conv2(x)
        x = self.bn2(x, training=training)
        x = tf.nn.relu(x)
        x = self.conv3(x)
        x = self.bn3(x, training=training)

        output = tf.nn.relu(tf.keras.layers.add([residual, x]))

        return output


def make_basic_block_layer(filter_num, blocks, stride=1):
    res_block = tf.keras.Sequential()
    res_block.add(BasicBlock(filter_num, stride=stride))

    for _ in range(1, blocks):
        res_block.add(BasicBlock(filter_num, stride=1))

    return res_block


def make_bottleneck_layer(filter_num, blocks, stride=1):
    res_block = tf.keras.Sequential()
    res_block.add(BottleNeck(filter_num, stride=stride))

    for _ in range(1, blocks):
        res_block.add(BottleNeck(filter_num, stride=1))

    return res_block

class ResNetTypeI(tf.keras.Model):
    def __init__(self, layer_params):
        super(ResNetTypeI, self).__init__()

        self.conv1 = tf.keras.layers.Conv2D(filters=64,
                                            kernel_size=(7, 7),
                                            strides=2,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3),
                                               strides=2,
                                               padding="same")

        self.layer1 = make_basic_block_layer(filter_num=64,
                                             blocks=layer_params[0])
        self.layer2 = make_basic_block_layer(filter_num=128,
                                             blocks=layer_params[1],
                                             stride=2)
        self.layer3 = make_basic_block_layer(filter_num=256,
                                             blocks=layer_params[2],
                                             stride=2)
        self.layer4 = make_basic_block_layer(filter_num=512,
                                             blocks=layer_params[3],
                                             stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        self.fc = tf.keras.layers.Dense(units=56, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        output = self.fc(x)

        return output


class ResNetTypeII(tf.keras.Model):
    def __init__(self, layer_params):
        super(ResNetTypeII, self).__init__()
        self.conv1 = tf.keras.layers.Conv2D(filters=64,
                                            kernel_size=(7, 7),
                                            strides=2,
                                            padding="same")
        self.bn1 = tf.keras.layers.BatchNormalization()
        self.pool1 = tf.keras.layers.MaxPool2D(pool_size=(3, 3),
                                               strides=2,
                                               padding="same")

        self.layer1 = make_bottleneck_layer(filter_num=64,
                                            blocks=layer_params[0])
        self.layer2 = make_bottleneck_layer(filter_num=128,
                                            blocks=layer_params[1],
                                            stride=2)
        self.layer3 = make_bottleneck_layer(filter_num=256,
                                            blocks=layer_params[2],
                                            stride=2)
        self.layer4 = make_bottleneck_layer(filter_num=512,
                                            blocks=layer_params[3],
                                            stride=2)

        self.avgpool = tf.keras.layers.GlobalAveragePooling2D()
        self.fc = tf.keras.layers.Dense(units=56, activation=tf.keras.activations.softmax)

    def call(self, inputs, training=None, mask=None):
        x = self.conv1(inputs)
        x = self.bn1(x, training=training)
        x = tf.nn.relu(x)
        x = self.pool1(x)
        x = self.layer1(x, training=training)
        x = self.layer2(x, training=training)
        x = self.layer3(x, training=training)
        x = self.layer4(x, training=training)
        x = self.avgpool(x)
        output = self.fc(x)

        return output

def resnet_50():
    return ResNetTypeII(layer_params=[3, 4, 6, 3])

new_model = resnet_50()

input_layer = tf.keras.layers.InputLayer(input_shape=(101, 5998))
new_model.build((101, 5998))
new_model.summary()

new_model.set_weights(model.weights)

m = tf.keras.Sequential([
    hub.KerasLayer("https://tfhub.dev/tensorflow/resnet_50/classification/1")
])
m.build([None, 101, 5996])

"""# Real shit

# Data load
"""

X = np.load(X_FILE_PATH)
y = np.load(y_FILE_PATH)

X = np.swapaxes(X, 2, 1)
print(X.shape)
print(y.shape)

class Config:
  def __init__(self, dictionary):
    self.__dict__.update(dictionary)

"""# Preprocessing"""

print(y)
print(len(set(y)))

#one_hot_matrix = np.eye(len(set(y)))
idx_2_names = dict()
names_2_idx = dict()

model_y = list()
index = -1
for item in y:
  if item not in names_2_idx:
    index += 1
    names_2_idx[item] = index
    idx_2_names[index] = item

  idx = names_2_idx[item]
  model_y.append(idx)

model_y = np.array(model_y)

"""## Save dictionary
Don't forget to setup a new dictionary name!
"""

dictionary_file_path = BASE_FOLDER + "dictionary-final.json"
dictionary = json.dumps(idx_2_names) # 
dictionary_file = open(dictionary_file_path, "w")
dictionary_file.write(dictionary)
dictionary_file.close()

shuffled_idxes = np.arange(y.shape[0])
np.random.shuffle(shuffled_idxes)
print(shuffled_idxes)

x_mean = np.mean(X, axis=(0, 2))[None, :, None]
x_std = np.std(X, axis=(0, 2))[None,:,None]

model_x = (X - x_mean) / x_std

model_y = model_y[shuffled_idxes]
model_x = model_x[shuffled_idxes, :, :, None]
print(model_y.shape)

MEAN_FILE_PATH = BASE_FOLDER + "mean-normalization-final.npy"
STD_FILE_PATH = BASE_FOLDER + "std-normalization-final.npy"

np.save(MEAN_FILE_PATH, x_mean)
np.save(STD_FILE_PATH, x_std)

"""# Load and normalize"""

dictionary_file_path = BASE_FOLDER + "dictionary-final.json"
with open(dictionary_file_path, "r") as file: 
  content = file.read()
  idx_2_names = json.loads(content)
  file.close()

name_2_idx = {name: key for key, name in idx_2_names.items()}
x_mean = np.load(MEAN_FILE_PATH)
x_std = np.load(STD_FILE_PATH)

model_x = (X - x_mean) / x_std

model_y = np.array([name_2_idx[name] for name in y])
categorical_y = to_categorical(model_y)

shuffled_idxes = np.arange(y.shape[0])
np.random.shuffle(shuffled_idxes)

print(shuffled_idxes)
model_x = model_x[shuffled_idxes]
model_y = model_y[shuffled_idxes]
categorical_y = categorical_y[shuffled_idxes]

"""# Additional metrics"""

pip install extra_keras_metrics

from extra_keras_metrics import average_precision_at_k

def mAP(y_true, y_pred):

"""# Base network - Dense arhitecture"""

from tensorflow.keras.metrics import AUC, PrecisionAtRecall, Recall, MeanAbsolutePercentageError, mean_absolute_percentage_error, Precision

def dense_block(units, dropout_rate, layer_index):
  return Sequential([
    Dense(units, activation="relu"),
    Dropout(dropout_rate)
  ], name="Dense-Block-{}".format(layer_index))

def create_dense_network(input_shape, output_shape, config):
  inp = Input(shape=input_shape)
  model_pipeline = Flatten()(inp)

  for layer in range(1, config.layers+1):
    model_pipeline = dense_block(config.units, config.dropout_rate, layer)(model_pipeline)

  # embedding_model = Model(inp, model_pipeline)
  # In this state model_pipeline presents the embedding with dropout included
  print(model_pipeline)
  output = Dense(output_shape, activation="softmax", name="Output")(model_pipeline)

  model = Model(inp, output)
  model.compile(Adam(learning_rate=config.learning_rate), loss=config.loss, metrics=[config.metric, AUC(), Precision()])
  return model

# dense_run = wandb.init(
#   project="sound-identifier",
#   group="dense-network-0.5",
#   config={
#   "input_shape": (256, 128, 1),
#   "layers": 6,
#   "units": 300,
#   "dropout_rate": .2,

#   "optimizer": "adam",
#   "loss": "sparse_categorical_crossentropy",
#   "metric": "accuracy",
  
#   "epochs": 50,
#   "batch_size": 128,
#   "learning_rate": 1e-3,
#   "validation_split": .2,
# })

dense_model_config = Config({
  "input_shape": (256, 128, 1),
  "layers": 6,
  "units": 300,
  "dropout_rate": .2,

  "optimizer": "adam",
  "loss": "categorical_crossentropy",
  "metric": "accuracy",
  
  "epochs": 50,
  "batch_size": 128,
  "learning_rate": 1e-3,
  "validation_split": .2,
})# dense_run.config
dense_model = create_dense_network(dense_model_config.input_shape, 57, dense_model_config)
dense_model.summary()

"""## Dense network training"""

print(model_x.shape)
print(model_y.shape)
print(categorical_y.shape)

callbacks = [
  # WandbCallback(),
  EarlyStopping(
    monitor="val_loss",
    patience=3
  )
]
history = dense_model.fit(
  model_x, 
  categorical_y, 
  batch_size=dense_model_config.batch_size, 
  epochs=dense_model_config.epochs, 
  validation_split=dense_model_config.validation_split, 
  callbacks=callbacks
)
# dense_run.finish()

dense_model_file = BASE_FOLDER + "models/dense-0.0.1.h5"
dense_model.load_weights(dense_model_file)

"""# Alex net"""

def create_alex_net(input_shape, output_size, config):
  inp = Input(shape=input_shape)
  # embedding = alex_net_arhitecture(input_shape)(inp)

  model_pipeline = Conv2D(32, kernel_size=3, strides=2, padding="same")(inp)
  model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
  model_pipeline = Dropout(config.dropout)(model_pipeline)

  model_pipeline = Conv2D(64, kernel_size=3, strides=2, padding="same")(model_pipeline)
  model_pipeline = ZeroPadding2D(padding=((0,1),(0,1)))(model_pipeline)
  model_pipeline = BatchNormalization(momentum=.8)(model_pipeline)
  model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
  model_pipeline = Dropout(config.dropout)(model_pipeline)

  model_pipeline = Conv2D(128, kernel_size=3, strides=2, padding="same")(model_pipeline)
  model_pipeline = BatchNormalization(momentum=.8)(model_pipeline)
  model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
  model_pipeline = Dropout(config.dropout)(model_pipeline)

  model_pipeline = Conv2D(256, kernel_size=3, strides=1, padding="same")(model_pipeline)
  model_pipeline = BatchNormalization(momentum=.8)(model_pipeline)
  model_pipeline = LeakyReLU(alpha=.2)(model_pipeline)
  model_pipeline = Dropout(config.dropout)(model_pipeline)

  model_pipeline = Flatten()(model_pipeline)
  embedding = Dense(300, activation="relu")(model_pipeline)
  model_pipeline = Dropout(config.dropout)(embedding)

  output = Dense(output_size, activation="softmax")(model_pipeline)

  model = Model(inp, output)
  model.compile(Adam(learning_rate=config.learning_rate), loss=config.loss, metrics=[config.metric, AUC(), Precision()])
  return model

# Start a run, tracking hyperparameters
# run = wandb.init(
#   project="sound-identifier",
#   group="alex-net-0.4",
#   config={
#     "input_shape": (256, 128, 1),
#     "dropout": .3,
#     "optimizer": "adam",
#     "loss": "sparse_categorical_crossentropy",
#     "metric": "accuracy",
#     "epochs": 50,
#     "batch_size": 128,
#     "learning_rate": 1e-4,
#     "validation_split": .2
#   })
config = Config({
    "input_shape": (256, 128, 1),
    "dropout": .3,
    "optimizer": "adam",
    "loss": "categorical_crossentropy",
    "metric": "accuracy",
    "epochs": 50,
    "batch_size": 128,
    "learning_rate": 1e-4,
    "validation_split": .2
  })# wandb.config

alexnet = create_alex_net(config.input_shape, 57, config)
alexnet_file_path = BASE_FOLDER + "models/alexnex-final.h5"
alexnet.load_weights(alexnet_file_path)

callbacks = [
  WandbCallback(),
  EarlyStopping(
    monitor="val_loss",
    patience=3
  )
]
history = alexnet.fit(model_x, model_y, batch_size=config.batch_size, epochs=config.epochs, validation_split=config.validation_split, callbacks=callbacks)
run.finish()

alexnet_file_path = BASE_FOLDER + "models/alexnex-final.h5"
alexnet.save(alexnet_file_path)

"""# ResNet-50"""

def base_conv(kernl_size, filters, stride, padding="valid"):
  return Sequential([
    Conv2D(filters, (kernl_size, kernl_size), strides=(stride, stride), padding=padding, kernel_regularizer=l2(0.001)),
    BatchNormalization(),
  ])

def conv(kernl_size, filters, stride, padding="valid"):
  return Sequential([
    base_conv(kernl_size, filters, stride, padding=padding),
    Activation('relu')
  ])

def conv_block(inp, filter1, filter2, stride):
  base_pipeline = conv(1, filter1, stride)(inp)
  base_pipeline = conv(3, filter1, 1, padding="same")(base_pipeline)
  base_pipeline = base_conv(1, filter2, 1)(base_pipeline)

  short_cut = base_conv(1, filter2, stride)(inp)

  output = Add()([short_cut, base_pipeline])
  output = Activation('relu')(output)
  return output

def identity_block(inp, filter1, filter2):
  flow = conv(1, filter1, 1)(inp)
  flow = conv(3, filter1, 1, padding="same")(flow)
  flow = base_conv(1, filter2, 1)(flow)

  output = Add()([flow, inp])
  output = Activation('relu')(output)
  return output
  

def create_resnet_50(input_shape, output_shape, config):
  inp = Input(shape=input_shape)

  flow = conv(7, 64, 2)(inp)
  flow = MaxPooling2D((3, 3), strides=(2, 2))(flow)

  flow = conv_block(flow, 64, 256, 1)
  flow = identity_block(flow, 64, 256)
  flow = identity_block(flow, 64, 256)

  flow = conv_block(flow, 128, 512, 2)
  flow = identity_block(flow, 128, 512)
  flow = identity_block(flow, 128, 512)
  flow = identity_block(flow, 128, 512)

  flow = conv_block(flow, 256, 1024, 2)
  flow = identity_block(flow, 256, 1024)
  flow = identity_block(flow, 256, 1024)
  flow = identity_block(flow, 256, 1024)
  flow = identity_block(flow, 256, 1024)
  flow = identity_block(flow, 256, 1024)

  flow = conv_block(flow, 512, 2048, 2)
  flow = identity_block(flow, 512, 2048)
  flow = identity_block(flow, 512, 2048)

  # ends with average pooling and dense connection
  flow = AveragePooling2D((2, 2), padding='same')(flow)
  flow = Flatten()(flow)
  flow = Dropout(config.dropout_rate)(flow)

  output = Dense(output_shape, activation="softmax")(flow)
  model = Model(inp, output)
  model.compile(Adam(config.learning_rate), loss=config.loss, metrics=[config.metric, AUC(), Precision()])
  return model

# resnet_34_run = wandb.init(
#   project="sound-identifier",
#   group="resnet-34-0.0.6",
#   config={
#     "input_shape": (256, 128, 1),
#     "dropout_rate": .25,
    
#     "optimizer": "adam",
#     "loss": "categorical_crossentropy",
#     "metric": "accuracy",

#     "patience": 5,
#     "epochs": 200,
#     "batch_size": 128,
#     "learning_rate": 1e-4,
#     "validation_split": .2
#   })
# resnet34_config = wandb.config
resnet_model_file = BASE_FOLDER + "models/resnet50-final.h5"
resnet34_config = Config({
  "input_shape": (256, 128, 1),
  "dropout_rate": .25,
  
  "optimizer": "adam",
  "loss": "categorical_crossentropy",
  "metric": "accuracy",

  "patience": 5,
  "epochs": 200,
  "batch_size": 128,
  "learning_rate": 1e-4,
  "validation_split": .2
})
resnet_model = create_resnet_50(resnet34_config.input_shape, 57, resnet34_config)
resnet_model.load_weights(resnet_model_file)
resnet_model.summary()

print(model_x.shape)
print(model_y.shape)

categorical_y = to_categorical(model_y)
print(categorical_y.shape)

callbacks = [
  # WandbCallback(),
  EarlyStopping(
    monitor="val_loss",
    patience=resnet34_config.patience
  )
]
history = resnet_model.fit(
  model_x, 
  categorical_y, 
  batch_size=resnet34_config.batch_size, 
  epochs=10, 
  validation_split=resnet34_config.validation_split, 
  callbacks=callbacks
)
# resnet_34_run.finish()

resnet_model_file = BASE_FOLDER + "models/resnet50-final.h5"
resnet_model.save(resnet_model_file)

"""# Evaluation"""

X_val_file_path = "/content/drive/My Drive/ML/shazam/data/eval-X-2.npy" #"data/songs/eval-X.npy"
y_val_file_path = "/content/drive/My Drive/ML/shazam/data/eval-y-2.npy"#"data/songs/eval-y.npy"

X_val = np.load(X_val_file_path)
y_val = np.load(y_val_file_path)

X_val = np.swapaxes(X_val, 1, 2)

print(X_val.shape)
print(y_val.shape)

name_2_idx

name_2_idx["Carl Orff (André Rieu) - O Fortuna.wav"] = "11"

X_val = (X_val - x_mean) / x_std
y_val = np.array([name_2_idx[name] for name in y_val])[:, None]
y_val = to_categorical(y_val)

dense_eval = dense_model.evaluate(X_val, y_val)

alexnet_eval = alexnet.evaluate(X_val, y_val)

resnet_eval = resnet_model.evaluate(X_val, y_val)

"""## Confusion matrix"""

pred = resnet_model.predict(X_val)
results = np.argmax(pred, axis=-1)

y_res = np.argmax(y_val, axis=-1)

confutions_matrix = np.zeros((57, 57))

for tr, pr in zip(y_res, results):
  confutions_matrix[tr, pr] += 1

confutions_matrix /= 50

confutions_matrix

names = [idx_2_names[str(idx)] for idx in range(57)]

import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt

df_cm = pd.DataFrame(confutions_matrix, index=names, columns=names)
plt.figure(figsize = (13,10))
sn.heatmap(df_cm, annot=False)
plt.show()



import plotly.graph_objects as go
import plotly.figure_factory as ff

z = [[0.1, 0.3, 0.5, 0.2],
     [1.0, 0.8, 0.6, 0.1],
     [0.1, 0.3, 0.6, 0.9],
     [0.6, 0.4, 0.2, 0.2]]

x = ['healthy', 'multiple diseases', 'rust', 'scab']
y =  ['healthy', 'multiple diseases', 'rust', 'scab']

# change each element of z to type string for annotations
z_text = [[str(y) for y in x] for x in confutions_matrix]

# set up figure 
fig = ff.create_annotated_heatmap(confutions_matrix, x=names, y=names, annotation_text=z_text, colorscale='Viridis', )

# add title
fig.update_layout(title_text='<i><b>Confusion matrix</b></i>',
                  #xaxis = dict(title='x'),
                  #yaxis = dict(title='x')
                 )

# add custom xaxis title
fig.add_annotation(dict(font=dict(color="black",size=14),
                        x=0.5,
                        y=-0.15,
                        showarrow=False,
                        text="Predicted value",
                        xref="paper",
                        yref="paper"))

# add custom yaxis title
fig.add_annotation(dict(font=dict(color="black",size=14),
                        x=-0.35,
                        y=0.5,
                        showarrow=False,
                        text="Real value",
                        textangle=-90,
                        xref="paper",
                        yref="paper"))

# adjust margins to make room for yaxis title
fig.update_layout(margin=dict(t=200, l=200))


# add colorbar
fig['data'][0]['showscale'] = True
fig.show()





['aggrnyl', 'agsunset', 'algae', 'amp', 'armyrose', 'balance',
  'blackbody', 'bluered', 'blues', 'blugrn', 'bluyl', 'brbg',
  'brwnyl', 'bugn', 'bupu', 'burg', 'burgyl', 'cividis', 'curl',
  'darkmint', 'deep', 'delta', 'dense', 'earth', 'edge', 'electric',
  'emrld', 'fall', 'geyser', 'gnbu', 'gray', 'greens', 'greys',
  'haline', 'hot', 'hsv', 'ice', 'icefire', 'inferno', 'jet',
  'magenta', 'magma', 'matter', 'mint', 'mrybm', 'mygbm', 'oranges',
  'orrd', 'oryel', 'peach', 'phase', 'picnic', 'pinkyl', 'piyg',
  'plasma', 'plotly3', 'portland', 'prgn', 'pubu', 'pubugn', 'puor',
  'purd', 'purp', 'purples', 'purpor', 'rainbow', 'rdbu', 'rdgy',
  'rdpu', 'rdylbu', 'rdylgn', 'redor', 'reds', 'solar', 'spectral',
  'speed', 'sunset', 'sunsetdark', 'teal', 'tealgrn', 'tealrose',
  'tempo', 'temps', 'thermal', 'tropic', 'turbid', 'twilight',
  'viridis', 'ylgn', 'ylgnbu', 'ylorbr', 'ylorrd']



import plotly.figure_factory as ff


colorscale = [[0, 'navy'], [1, 'plum']]
font_colors = ['white', 'black']
fig = ff.create_annotated_heatmap(
    confutions_matrix,
    x=names,
    y=names
)
fig.show()

import plotly.graph_objects as go
import datetime
import numpy as np

fig = go.Figure(data=go.Heatmap(
    z=confutions_matrix,
    x=names,
    y=names,
    colorscale='ylorrd'
  ),
)


# fig.update_layout(
#   title='Confution matrix'
# )
fig.show()











y[:10]

index = 2300
name = idx_2_names[model_y[index]]
result = model.predict(np.array([model_x[index]]))

print(np.argmax(result))
print(idx_2_names[np.argmax(result)])
print(model_y[index])
print(idx_2_names[model_y[index]])

for _ in range(10):
  index = np.random.randint(0, y.shape[0])
  example = np.array([model_x[index]])
  result = model.predict(example)
  result = np.argmax(result)

  print(idx_2_names[model_y[index]])
  print(idx_2_names[result])
  print()

lunca = np.load(lunca_FILE_PATH)
lunca = np.array([lunca])
lunca = (lunca - x_mean) / x_std

result = model.predict(lunca[0])
print(np.argmax(result, axis=-1))

idx_2_names[20]

MODEL_FILE_PATH = BASE_FOLDER + "models/alex-net-0.3.h5"
model.save_weights(MODEL_FILE_PATH)



"""# RNN MODEL"""

from tensorflow.keras.layers import GRU

# Start a run, tracking hyperparameters
wandb.init(
  project="sound-identifier",
  group="gru-0.1",
  config={
    "input_shape": (256, 128),
    "dropout": .3,
    "optimizer": "adam",
    "loss": "sparse_categorical_crossentropy",
    "metric": "accuracy",
    "epochs": 50,
    "batch_size": 128,
    "learning_rate": 1e-4,
    "validation_split": .2
  })
config = wandb.config

def create_rnn_model(input_shape, output_shape, config):
  input_layer = Input(shape=input_shape)

  model_pipeline = GRU(300, dropout=config.dropout, return_sequences=True)(input_layer)
  model_pipeline = GRU(300, dropout=config.dropout, return_sequences=True)(model_pipeline)
  model_pipeline = GRU(300, dropout=config.dropout)(model_pipeline)
  model_pipeline = Dense(300, activation="relu")(model_pipeline)
  model_pipeline = Dropout(config.dropout)(model_pipeline)

  output_layer = Dense(output_shape, activation="softmax")(model_pipeline)

  model = Model(input_layer, output_layer)
  model.compile(Adam(learning_rate=config.learning_rate), loss=config.loss, metrics=[config.metric])
  return model

rnn_model = create_rnn_model(config.input_shape, 57, config)

rnn_model_x = model_x[:,:,:,0]
rnn_model_x = np.swapaxes(rnn_model_x, 1, 2)
print(rnn_model_x.shape)

callbacks = [
  WandbCallback(),
  EarlyStopping(
    monitor="val_loss",
    patience=3
  )
]
history = rnn_model.fit(rnn_model_x, model_y, batch_size=config.batch_size, epochs=config.epochs, validation_split=config.validation_split, callbacks=callbacks)

rnn_model.summary()

rnn_model_file_path = BASE_FOLDER + "models/gru-0.1.h5"
rnn_model.save_weights(rnn_model_file_path)

