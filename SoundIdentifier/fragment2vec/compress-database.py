import os
import numpy as np


DATABASE_FOLDER = "../../data/songs/"

if __name__ == '__main__':
	X = np.empty((0, 128, 256))
	y = []

	for folder in os.listdir(DATABASE_FOLDER):
		if folder.startswith("."):
			continue

		file_path = lambda x: "{}{}/{}".format(DATABASE_FOLDER, folder, x)

		sub_x = np.load(file_path("eval-X-2.npy"))
		sub_y = np.load(file_path("eval-y-2.npy"))
		X = np.append(X, sub_x, axis=0)
		y.extend(sub_y)

	y = np.array(y)
	np.save("eval-X-2.npy", X)
	np.save("eval-y-2.npy", y, allow_pickle=True)
