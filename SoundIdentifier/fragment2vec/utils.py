import os
from pydub import AudioSegment


SONG_FOLDER = "../data/songs/"
NOISE_FOLDER = "../../data/Ambient noise/"


def load_song_folder(initial_folder=SONG_FOLDER):
	print("Loading songs")
	song_folders = [sub_folder for sub_folder in os.listdir(SONG_FOLDER) if os.path.isdir(initial_folder) and (not sub_folder.startswith("."))]
	songs = list()
	for folder in song_folders:
		new_folder = "{}{}/".format(initial_folder, folder)
		print(new_folder)
		songs.extend(load_in_folder_songs(new_folder))
	return songs


def load_in_folder_songs(folder, recursive=False):
	songs = []
	for item in os.listdir(folder):
		if recursive and os.path.isdir(folder + item):
			songs.extend(load_in_folder_songs(folder + item + "/", recursive=recursive))
		elif item.endswith(".wav"):
			songs.append((item, AudioSegment.from_wav(folder + item)))
	return songs


def load_noises(folder=NOISE_FOLDER):
	print("Loading noises")
	return load_in_folder_songs(folder)