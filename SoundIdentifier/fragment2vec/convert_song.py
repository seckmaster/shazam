# import matplotlib
# matplotlib.use('Agg')
# import os
import librosa
import matplotlib.pyplot as plt
# from scipy.io import wavfile
# from scipy import signal
# import cv2


import numpy as np


def file_to_spectrogram(wav_file):
  y, sr = librosa.load(wav_file)
  # TODO (128, len(samples)/431)
  melSpec = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=128, hop_length=431)
  melSpec_dB = librosa.power_to_db(melSpec, ref=np.max)
  return melSpec_dB
