import os
import sys
from random import random

import numpy as np
from typing import List, Tuple, NewType

from time import time
from convert_song import file_to_spectrogram
from utils import load_in_folder_songs, load_noises

SONG_FOLDER = "../data/songs/"
NOISE_FOLDER = "../data/Ambient noise/"
Dataset = NewType("Dataset", Tuple[List[np.ndarray], List[str]])


def random_fragment(audio, duration):
	audio_length = len(audio)
	audio_start = np.random.randint(0, audio_length - duration)
	return audio[audio_start:audio_start + duration]


def create_random_fragment(song, noise, change_ampl=True, duration=5000):
	song_fragment = random_fragment(song, duration)
	noise_fragment = random_fragment(noise, duration)

	song_fragment += np.random.randint(-5, 5)
	noise_fragment += np.random.randint(-5, 5)

	song_fragment = song_fragment.overlay(noise_fragment)
	return song_fragment


def create_database(songs, noises, dir, examples_per_song=200):
	X, y = list(), list()
	temporary_file = dir + "temporary.wav"

	for song_name, song in songs:
		print(song_name)
		for _ in range(examples_per_song):
			noise_index = np.random.randint(0, len(noises))
			_, noise = noises[noise_index]

			fragment = create_random_fragment(song, noise)
			fragment.export(temporary_file, format="wav")
			fragment = file_to_spectrogram(temporary_file)

			X.append(fragment)
			y.append(song_name)

	os.remove(temporary_file)
	return X, y


def create_dataset(songs, noises, sample_duration=5000, examples_per_song=50, noises_per_song_range=(5, 20)):
	X, y = list(), list()
	temporary_file = str(np.random.randint(0, 10000)) + "-temporary.wav"

	for song_name, song in songs:
		print(song_name)

		for idx in range(examples_per_song):
			song_fragment = random_fragment(song, duration=sample_duration) + 4

			random_noises_count = np.random.randint(noises_per_song_range[0], noises_per_song_range[1])

			noise_fragment = None

			for idx2 in range(random_noises_count):
				random_noise_index = np.random.randint(len(noises))
				noise_name, noise = noises[random_noise_index]
				print(idx, "/", examples_per_song, ",", idx2 + 1, "/", random_noises_count, noise_name, song_name)

				new_noise_fragment = random_fragment(noise, sample_duration)
				if noise_fragment:
					noise_fragment = noise_fragment.overlay(new_noise_fragment)
				else:
					noise_fragment = new_noise_fragment

			song_fragment = song_fragment.overlay(noise_fragment)

			print("Saving ...")
			song_fragment.export(temporary_file, format="wav")
			spectrogram = file_to_spectrogram(temporary_file)

			X.append(spectrogram)
			y.append(song_name)
			print()

	os.remove(temporary_file)
	return X, y


if __name__ == '__main__':
	dir = sys.argv[1] + "/"

	start_time = time()

	songs = load_in_folder_songs(dir)
	print([song for song, _ in songs])
	noises = load_noises()

	# X, y = create_database(songs, noises, dir)

	X, y = create_dataset(songs, noises)

	print(time() - start_time)

	X = np.array(X)
	y = np.array(y)

	np.save(dir+"eval-X-2", X)
	np.save(dir+"eval-y-2", y, allow_pickle=True)

