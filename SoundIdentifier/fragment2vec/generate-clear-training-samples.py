import os
import sys
import numpy as np
from typing import List, Tuple, NewType

from time import time
from convert_song import file_to_spectrogram
from utils import load_song_folder, load_in_folder_songs

Dataset = NewType("Dataset", Tuple[List[np.ndarray], List[str]])
TEMPORARY_FILE = "temporary.wav"

def split_song_on_fragments(song, fragment_duration=5000, padding=2500):
	song_duration = len(song)
	fragments = list()
	fragment_start, fragment_end = 0, fragment_duration

	while fragment_end < song_duration:
		fragments.append(song[fragment_start:fragment_end])
		fragment_start += padding
		fragment_end += padding

	return fragments


def split_songs_on_fragments(songs):
	X, y = list(), list()

	for name, song in songs:
		song_fragments = split_song_on_fragments(song)
		y.extend([name] * len(song_fragments))
		X.extend(song_fragments)

	return X, y


def convert_fragment_to_spectrogram(fragment, temporary_file=TEMPORARY_FILE):
	fragment.export(temporary_file, format="wav")
	return file_to_spectrogram(temporary_file)


def convert_fragments_to_spectrogram(fragments, temporary_file=TEMPORARY_FILE):
	return [
		convert_fragment_to_spectrogram(fragment, temporary_file=temporary_file)
		for fragment in fragments
	]


if __name__ == '__main__':
	directory = sys.argv[1] + "/"
	print("Loading songs from directory: {}".format(directory))
	songs = load_in_folder_songs(directory)
	print("Splitting songs on fragments")
	fragments, fragment_names = split_songs_on_fragments(songs)
	print("Converting fragments to spectrogram")
	fragments = convert_fragments_to_spectrogram(fragments)
	os.remove(TEMPORARY_FILE)
	print("Saving data")

	fragments = np.array(fragments)
	fragment_names = np.array(fragment_names)
	np.save(directory + "clear-fragments", fragments)
	np.save(directory + "clear-fragment-names", fragment_names, allow_pickle=True)

