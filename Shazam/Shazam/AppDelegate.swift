//
//  AppDelegate.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let window = UIWindow(frame: UIScreen.main.bounds)
    window.makeKeyAndVisible()
    let viewController = MainViewController(delegate: self)
    window.setRootViewController(viewController, animated: false)
    self.window = window
    return true
  }
}

extension AppDelegate: MainRouterDelegate {
  func dismiss(MainViewController viewController: MainViewController) {
  }
}

extension UIWindow {
  var topViewController: UIViewController? {
    func topViewController(with rootViewController: UIViewController?) -> UIViewController? {
      if rootViewController == nil { return nil }
      
      if let tabBarController = rootViewController as? UITabBarController {
        return topViewController(with: tabBarController.selectedViewController)
      } else if let navigationController = rootViewController as? UINavigationController {
        return topViewController(with: navigationController.visibleViewController)
      } else if rootViewController?.presentedViewController != nil {
        return topViewController(with: rootViewController?.presentedViewController)
      }
      return rootViewController
    }
    return topViewController(with: rootViewController)
  }
  
  func setRootViewController(_ viewController: UIViewController, animated: Bool, completion: ((Bool) -> Void)? = nil) {
    switch animated {
    case true:
      rootViewController = viewController
      UIView.transition(with: self, duration: 0.3, options: [.transitionCrossDissolve], animations: {}, completion: completion)
    case false:
      rootViewController = viewController
      completion?(true)
    }
  }
  
  func disableDarkMode() {
    if #available(iOS 13.0, *) {
      overrideUserInterfaceStyle = .light
    }
  }
}
extension UIColor {
  static let regaloPurple = UIColor(red: 0.655, green: 0.02, blue: 0.922, alpha: 1.0)
  static let regaloPurpleLight = UIColor(red: 0.849, green: 0.483, blue: 1.0, alpha: 1.0)
  static let regaloTextWhite = UIColor.white
  static let regaloOffWhite = UIColor(red: 0.902, green: 0.906, blue: 0.91, alpha: 1.0)
  static let regaloTextGray = UIColor(red: 0.706, green: 0.714, blue: 0.733, alpha: 1.0)
  static let regaloTextPlaceholder = UIColor(red: 0.51, green: 0.522, blue: 0.553, alpha: 1.0)
  static let regaloBorderColor = UIColor(red: 0.224, green: 0.239, blue: 0.298, alpha: 1.0)
  static let regaloBackground = UIColor(red: 0.102, green: 0.11, blue: 0.133, alpha: 1.0)
  static let regaloRed = UIColor(red: 1.0, green: 0.363, blue: 0.401, alpha: 1.0)
  static let regaloBackgroundLight = UIColor(red: 0.149, green: 0.161, blue: 0.2, alpha: 1.0)
  static let regaloFacebook = UIColor(red: 59, green: 89, blue: 152)
  static let regaloGoogle = UIColor(red: 220, green: 78, blue: 65)
  static let regaloGreen = UIColor(red: 145, green: 241, blue: 139)
  static let regaloMidnightExpress = UIColor(red: 38, green: 41, blue: 51)
  static let regaloMidnightExpressLight = UIColor(red: 57, green: 61, blue: 76)
  
  convenience init(red: Int, green: Int, blue: Int, alpha: CGFloat = 1) {
    self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: alpha)
  }
}
