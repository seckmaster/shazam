//
//  MainInteractor.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//  
//

import Foundation

protocol MainBusinessLogic {
  func startRecording()
  func stopRecording()
  func playSound(_ data: Data)
  func uploadSound(_ data: Data)
}

class MainInteractor {
  var presenter: MainPresentationLogic?
  lazy var service: AudioService = .init()
  lazy var api: UploadFileApi = .init()
}

// MARK: - Business Logic
extension MainInteractor: MainBusinessLogic {
  func startRecording() {
    service
      .initializeSession()
      .map(with: service.startRecording)
      .observe { [weak self] in self?.presenter?.presentStartRecordingResult($0) }
  }
  
  func stopRecording() {
    service
      .finishRecording()
      .observe { [weak self] in self?.presenter?.presentStartFinishRecordingResult($0) }
  }
  
  func playSound(_ data: Data) {
    do {
      try service.playSound(data)
    } catch {
      print(error)
    }
  }
  
  func uploadSound(_ data: Data) {
    api
      .uploadFile(data: data)
      .observe { [weak self] in self?.presenter?.presentUploadResult($0) }
  }
}
