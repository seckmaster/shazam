//
//  MainPresenter.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//  
//

import Foundation

protocol MainPresentationLogic {
  func presentStartRecordingResult(_ result: Result<(), Error>)
  func presentStartFinishRecordingResult(_ result: Result<Data, Error>)
  func presentUploadResult(_ result: Result<[String: Float], Error>)
}

class MainPresenter {
  weak var viewController: MainDisplayLogic?
}

// MARK: - Presentation Logic
extension MainPresenter: MainPresentationLogic {
  func presentStartRecordingResult(_ result: Result<(), Error>) {
    switch result {
    case .success:
      viewController?.didStartRecording()
    case .failure(let error):
      viewController?.cannotStartRecording(error: error)
    }
  }
  
  func presentStartFinishRecordingResult(_ result: Result<Data, Error>) {
    switch result {
    case .success(let data):
      viewController?.didStopRecording(data: data)
    case .failure(let error):
      viewController?.didStopRecording(error: error)
    }
  }
  
  func presentUploadResult(_ result: Result<[String: Float], Error>) {
    switch result {
    case .success(let response):
      let results = response
        .map {  ($0.key, $0.value) }
        .sorted { $0.1 > $1.1 }
        .map { "🎵" + $0.0 }
        .prefix(5)
        .joined(separator: "\n")
      viewController?.displayUploadSuccess(results: results)
    case .failure(let error):
      viewController?.displayUploadFailure(error)
    }
  }
}
