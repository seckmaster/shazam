//
//  AudioService.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//

import Foundation
import AVFoundation
import PovioKit

final class AudioService: NSObject {
  private let session: AVAudioSession
  private var player: AVAudioPlayer!
  private var recorder: AVAudioRecorder!
  private var promise: Promise<Data>!
  
  init(
    session: AVAudioSession = .sharedInstance()
  ) {
    self.session = session
  }
}

extension AudioService {
  func initializeSession() -> Promise<()> {
    do {
      try session.setCategory(.record, mode: .default)
      try session.setActive(true)
      return Promise { seal in
        session.requestRecordPermission {
          switch $0 {
          case true:
            seal.resolve()
          case false:
            seal.reject(with: Error.notAllowed)
          }
        }
      }
    } catch {
      return .error(error)
    }
  }
  
  func startRecording() throws {
    let settings = [
      AVFormatIDKey: Int(kAudioFormatFLAC),
      AVSampleRateKey: 44100,
      AVNumberOfChannelsKey: 1,
      AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
    ]
    
    do {
      recorder = try AVAudioRecorder(url: filePath(), settings: settings)
      recorder.delegate = self
      recorder.record()
    } catch {
      throw error
    }
  }
  
  func finishRecording() -> Promise<Data> {
    precondition(recorder != nil, "Call `startRecording` before `finishRecording`!")
    recorder.stop()
    promise = .init()
    return promise
  }
  
  func playSound(_ data: Data) throws {
    player = try AVAudioPlayer(data: data)
    player.play()
  }
  
  func stopPlaying() {
    player.stop()
    player = nil
  }
  
  private func filePath() -> URL {
    getDocumentsDirectory().appendingPathComponent("recording.flac")
  }
  
  private func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
  }
}

extension AudioService: AVAudioRecorderDelegate {
  func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    print("audioRecorderDidFinishRecording", flag)
    guard flag else { return }
    do {
      try session.setCategory(.playback)
      let data = try Data(contentsOf: filePath())
      promise.resolve(with: data)
      promise = nil
    } catch {
      promise.reject(with: error)
      promise = nil
    }
  }
  
  
  private func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
    print("audioRecorderEncodeErrorDidOccur", error?.localizedDescription ?? "")
    promise.reject(with: error ?? .unknown)
    promise = nil
  }
}

extension AudioService {
  enum Error: Swift.Error {
    case notAllowed
    case unknown
  }
}
