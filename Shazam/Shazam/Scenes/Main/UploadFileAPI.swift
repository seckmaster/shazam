//
//  UploadFileAPI.swift
//  Shazam
//
//  Created by Toni Kocjan on 05/12/2020.
//

import PovioKit

class UploadFileApi {
  private let client: AlamofireNetworkClient
  
  init(client: AlamofireNetworkClient = .init()) {
    self.client = client
  }
  
  func uploadFile(
    data: Data,
    filename: String = "audio_recording.flac",
    mimetype: String = "audio/flac"
  ) -> Promise<[String: Float]> {
    client
      .upload(
        method: .put,
        endpoint: "http://6fa7257748fa.ngrok.io/upload/\(filename)",
        data: data,
        name: "audio",
        fileName: filename,
        mimeType: mimetype)
      .validate(statusCode: 200..<300)
      .decode([String: Float].self, decoder: .init())
  }
}
