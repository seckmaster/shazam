//
//  MainContentView.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//  
//

import UIKit
import Pulsator
import SnapKit
import PovioKit

class MainContentView: UIView {
  let shazamButton = UIButton()
  let pulsator = Pulsator()
  let timeLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    shazamButton.layer.cornerRadius = shazamButton.frame.height * 0.5
  }
}

// MARK: - Private Methods
private extension MainContentView {
  func setupView() {
    backgroundColor = .regaloBackground
    setupShazamButton()
    setupPulsatorView()
    setupTimeLabel()
  }
  
  func setupShazamButton() {
    addSubview(shazamButton)
    shazamButton.backgroundColor = .regaloFacebook
    shazamButton.setImage(UIImage(named: "wave4"), for: .normal)
    shazamButton.setImage(UIImage(named: "wave4"), for: .highlighted)
    shazamButton.layer.zPosition = 1
    shazamButton.imageEdgeInsets = .init(all: 70)
    shazamButton.snp.makeConstraints {
      $0.center.equalToSuperview()
      $0.size.equalTo(230)
    }
  }
  
  func setupPulsatorView() {
    let view = UIView()
    addSubview(view)
    view.layer.addSublayer(pulsator)
    pulsator.numPulse = 10
    pulsator.backgroundColor = UIColor.regaloFacebook.cgColor
    pulsator.animationDuration = 8
    pulsator.radius = 600
    pulsator.fromValueForRadius = 1
    view.snp.makeConstraints {
      $0.leading.equalTo(shazamButton.snp.centerX)
      $0.top.equalTo(shazamButton.snp.centerY)
    }
  }
  
  func setupTimeLabel() {
    addSubview(timeLabel)
    timeLabel.font = .systemFont(ofSize: 24)
    timeLabel.textColor = .white
    timeLabel.text = "0s"
    timeLabel.snp.makeConstraints {
      $0.centerX.equalToSuperview()
      $0.top.equalTo(180)
    }
  }
}
