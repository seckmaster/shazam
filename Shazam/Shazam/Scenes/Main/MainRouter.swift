//
//  MainRouter.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//  
//

import Foundation

protocol MainRoutingLogic {
  func dismiss()
  func navigateToResults(_ results: String)
}

protocol MainRouterDelegate: AnyObject {
  func dismiss(MainViewController viewController: MainViewController)
}

class MainRouter {
  weak var viewController: MainViewController?
  weak var delegate: MainRouterDelegate?
}

// MARK: - Routing Logic
extension MainRouter: MainRoutingLogic {
  func dismiss() {
    viewController.map { delegate?.dismiss(MainViewController: $0) }
  }
  
  func navigateToResults(_ results: String) {
    let dialog = DialogViewController(results: results, delegate: self)
    viewController?.present(dialog, animated: true)
  }
}

extension MainRouter: DialogRouterDelegate {
  func dismiss(DialogViewController viewController: DialogViewController) {
    viewController.dismiss(animated: true)
  }
}
