//
//  MainViewController.swift
//  Shazam
//
//  Created by Toni Kocjan on 22/11/2020.
//  
//

import UIKit
import PovioKit

protocol MainDisplayLogic: AnyObject {
  func didStartRecording()
  func cannotStartRecording(error: Error)
  func didStopRecording(data: Data)
  func didStopRecording(error: Error)
  func displayUploadSuccess(results: String)
  func displayUploadFailure(_ error: Error)
}

class MainViewController: UIViewController {
  override var preferredStatusBarStyle: UIStatusBarStyle {
    .lightContent
  }
  
  var interactor: MainBusinessLogic?
  var router: MainRoutingLogic?
  private lazy var contentView = MainContentView()
  private let timer = DispatchTimer()
  
  init(delegate: MainRouterDelegate?) {
    super.init(nibName: nil, bundle: nil)
    let interactor = MainInteractor()
    let presenter = MainPresenter()
    let router = MainRouter()
    interactor.presenter = presenter
    presenter.viewController = self
    router.viewController = self
    router.delegate = delegate
    self.interactor = interactor
    self.router = router
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    view = contentView
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }
}

// MARK: - Display Logic
extension MainViewController: MainDisplayLogic {
  func didStartRecording() {
    contentView.pulsator.start()
    var time = 0
    self.contentView.timeLabel.text = "0s"
    timer.schedule(interval: .seconds(1), repeating: true, on: .main) {
      time += 1
      self.contentView.timeLabel.text = String(time) + "s"
      if time == 15 {
        self.interactor?.stopRecording()
        self.timer.stop()
      }
    }
  }
  
  func cannotStartRecording(error: Error) {
    print("Cannot start recording due to", error)
  }
  
  func didStopRecording(data: Data) {
//    interactor?.playSound(data)
    interactor?.uploadSound(data)
    contentView.pulsator.stop()
  }
  
  func didStopRecording(error: Error) {
    print("Recording failed due to", error)
    contentView.pulsator.stop()
    timer.stop()
  }
  
  func displayUploadSuccess(results: String) {
    router?.navigateToResults(results)
  }
  
  func displayUploadFailure(_ error: Error) {
    print("failure")
  }
}

// MARK: - Private Methods
private extension MainViewController {
  func setupViews() {
    setupContentView()
  }
  
  func setupContentView() {
    contentView.shazamButton.addTarget(self, action: #selector(didTapShazamButton), for: .touchUpInside)
  }
  
  @objc func didTapShazamButton() {
    switch contentView.pulsator.isPulsating {
    case true:
      interactor?.stopRecording()
    case false:
      interactor?.startRecording()
    }
  }
  
  func manipulatePulsating() {
    let pulses = Int.random(in: 8...12)
    let updateEveryS = 0.01
    increaseNumPulses(to: pulses, updateEveryS: updateEveryS)
    let minDelay = updateEveryS * TimeInterval(abs(pulses - contentView.pulsator.numPulse))
    let adjustDelay = TimeInterval.random(in: minDelay...(minDelay + 2))
    DispatchQueue.main.asyncAfter(deadline: .now() + adjustDelay, execute: manipulatePulsating)
  }
  
  func increaseNumPulses(to: Int, updateEveryS: TimeInterval) {
    guard contentView.pulsator.numPulse != to else { return }
    let increase = contentView.pulsator.numPulse < to ? 1 : -1
    contentView.pulsator.numPulse += increase
    DispatchQueue.main.asyncAfter(deadline: .now() + updateEveryS) {
      self.increaseNumPulses(to: to, updateEveryS: updateEveryS)
    }
  }
}
