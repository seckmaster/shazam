//
//  DialogRouter.swift
//  Shazam
//
//  Created by Toni Kocjan on 03/01/2021.
//  
//

import Foundation

protocol DialogRoutingLogic {
  func dismiss()
}

protocol DialogRouterDelegate: AnyObject {
  func dismiss(DialogViewController viewController: DialogViewController)
}

class DialogRouter {
  weak var viewController: DialogViewController?
  weak var delegate: DialogRouterDelegate?
}

// MARK: - Routing Logic
extension DialogRouter: DialogRoutingLogic {
  func dismiss() {
    viewController.map { delegate?.dismiss(DialogViewController: $0) }
  }
}
