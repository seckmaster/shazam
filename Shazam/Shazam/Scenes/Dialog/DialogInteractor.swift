//
//  DialogInteractor.swift
//  Shazam
//
//  Created by Toni Kocjan on 03/01/2021.
//  
//

import Foundation

protocol DialogBusinessLogic {
}

class DialogInteractor {
  var presenter: DialogPresentationLogic?
}

// MARK: - Business Logic
extension DialogInteractor: DialogBusinessLogic {
}
