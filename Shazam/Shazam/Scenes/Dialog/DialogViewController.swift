//
//  DialogViewController.swift
//  Shazam
//
//  Created by Toni Kocjan on 03/01/2021.
//  
//

import UIKit

protocol DialogDisplayLogic: AnyObject {
}

class DialogViewController: UIViewController {
  var interactor: DialogBusinessLogic?
  var router: DialogRoutingLogic?
  private lazy var contentView = DialogContentView()
  
  init(results: String, delegate: DialogRouterDelegate?) {
    super.init(nibName: nil, bundle: nil)
    let interactor = DialogInteractor()
    let presenter = DialogPresenter()
    let router = DialogRouter()
    interactor.presenter = presenter
    presenter.viewController = self
    router.viewController = self
    router.delegate = delegate
    self.interactor = interactor
    self.router = router
    self.modalTransitionStyle = .crossDissolve
    self.modalPresentationStyle = .overCurrentContext
    self.contentView.resultsLabel.text = results
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }
}

// MARK: - Display Logic
extension DialogViewController: DialogDisplayLogic {
}

// MARK: - Private Methods
private extension DialogViewController {
  func setupViews() {
    setupContentView()
  }
  
  func setupContentView() {
    view.addSubview(contentView)
    contentView.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }
    let gesture = UITapGestureRecognizer(target: self, action: #selector(didTapView))
    contentView.addGestureRecognizer(gesture)
  }
  
  @objc func didTapView() {
    router?.dismiss()
  }
}
