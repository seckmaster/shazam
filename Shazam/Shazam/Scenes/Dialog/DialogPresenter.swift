//
//  DialogPresenter.swift
//  Shazam
//
//  Created by Toni Kocjan on 03/01/2021.
//  
//

import Foundation

protocol DialogPresentationLogic {
}

class DialogPresenter {
  weak var viewController: DialogDisplayLogic?
}

// MARK: - Presentation Logic
extension DialogPresenter: DialogPresentationLogic {
}
