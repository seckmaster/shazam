//
//  DialogContentView.swift
//  Shazam
//
//  Created by Toni Kocjan on 03/01/2021.
//  
//

import UIKit

class DialogContentView: UIView {
  let backgroundView = UIView()
  let containerView = UIView()
  let titleLabel = UILabel()
  let separator = UIView()
  let resultsLabel = UILabel()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Private Methods
private extension DialogContentView {
  func setupView() {
    backgroundColor = .clear
    setupBackgroundView()
    setupContainerView()
    setupTitleLabel()
    setupResultsLabel()
  }
  
  func setupBackgroundView() {
    addSubview(backgroundView)
    backgroundView.backgroundColor = .black
    backgroundView.alpha = 0.3
    backgroundView.snp.makeConstraints {
      $0.edges.equalToSuperview()
    }
  }
  
  func setupContainerView() {
    addSubview(containerView)
    containerView.backgroundColor = .white
    containerView.layer.cornerRadius = 24
    containerView.snp.makeConstraints {
      $0.center.equalToSuperview()
      $0.leading.trailing.equalToSuperview().inset(40)
    }
  }
  
  func setupTitleLabel() {
    containerView.addSubview(titleLabel)
    titleLabel.textColor = .black
    titleLabel.text = "Predictions 🚀"
    titleLabel.textAlignment = .center
    titleLabel.snp.makeConstraints {
      $0.top.equalTo(12)
      $0.centerX.equalToSuperview()
    }
  }
  
  func setupSeparator() {
    containerView.addSubview(separator)
    separator.backgroundColor = .gray
    separator.snp.makeConstraints {
      $0.leading.trailing.equalToSuperview()
      $0.height.equalTo(1)
      $0.top.equalTo(titleLabel.snp.bottom).offset(12)
    }
  }
  
  func setupResultsLabel() {
    containerView.addSubview(resultsLabel)
    resultsLabel.textColor = .black
    resultsLabel.numberOfLines = 0
    resultsLabel.font = .systemFont(ofSize: 14)
    resultsLabel.snp.makeConstraints {
      $0.top.equalTo(titleLabel.snp.bottom).offset(23)
      $0.leading.trailing.bottom.equalToSuperview().inset(24)
    }
  }
}
